## Prerequisites 
- `rustup target add thumbv6m-none-eabi`
- `cargo install elf2uf2-rs`  (if you want to automatically convert and load the ELF to your macropad)


## Building

Run `cargo build` to build an ELF targeting `thumbv6m-none-eabi`.
To change targets specify with `--target <target>` when building or edit the `.cargo/config`

## Running and deploying

Run `cargo run` to convert the ELF to UF2 and automatically load it onto your macropad.
The macropad has to be accessible from your environment, i.e if using WSL you will need to mount the macropad running in bootloader mode.
