//! Blinks the LED on a Adafruit MacroPad board
//!
//! This will blink on-board LED.
#![no_std]
#![no_main]

use adafruit_macropad::{
    hal::{
        clocks::{init_clocks_and_plls, Clock},
        pac,
        watchdog::Watchdog,
        Sio, Spi, gpio::FunctionSpi
    },
    Pins, XOSC_CRYSTAL_FREQ,
};
use cortex_m_rt::{entry};
use embedded_hal::{digital::v2::OutputPin};
use embedded_time::rate::*;
use sh1106::{prelude::*, Builder, NoOutputPin};
use panic_halt as _;

#[entry]
fn main() -> ! {
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();

    let mut watchdog = Watchdog::new(pac.WATCHDOG);

    let clocks = init_clocks_and_plls(
        XOSC_CRYSTAL_FREQ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().integer());

    let sio = Sio::new(pac.SIO);
    let pins = Pins::new(
        pac.IO_BANK0, 
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );


    //display initilization 
    let _ = pins.sclk.into_mode::<FunctionSpi>();
    let _ = pins.mosi.into_mode::<FunctionSpi>();
    let _ = pins.miso.into_mode::<FunctionSpi>();
    let mut led_pin = pins.led.into_push_pull_output();
    let oled_dc_pin = pins.oled_dc.into_push_pull_output();
    let oled_cs_pin = pins.oled_cs.into_push_pull_output();

    //prevents oled from resetting
    let mut oled_reset_pin = pins.oled_reset.into_push_pull_output();
    let res = oled_reset_pin.set_high();
    res.unwrap();

    let spi: Spi<_,_ , 8> = Spi::new(pac.SPI1).init(&mut pac.RESETS,  clocks.peripheral_clock.freq(), 16_000_000u32.Hz(), &embedded_hal::spi::MODE_0);
    let mut display: GraphicsMode<_> = Builder::new().connect_spi(spi, oled_dc_pin, oled_cs_pin).into();

    display.init().unwrap();
    display.flush().unwrap();
    for x in 30..40 {
        display.set_pixel(x, 20, 1);
    }
    display.flush().unwrap();
    loop {
        
        led_pin.set_high().unwrap();
        delay.delay_ms(500);
        led_pin.set_low().unwrap();
        delay.delay_ms(500);
    }
}
